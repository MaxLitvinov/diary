package com.teadust.diary;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;

import com.teadust.diary.fragment.stack.FragmentStack;

public interface MainView {

    Toolbar getToolbar();
    DrawerLayout getDrawerLayout();
    ActionBarDrawerToggle getDrawerToggle();
    FragmentStack getFragmentStack();

    void initUI();

    void showNoteList();

    void onNoteAddClick();

    void showNoteAddFragment();
}
