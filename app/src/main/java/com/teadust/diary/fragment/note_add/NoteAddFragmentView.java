package com.teadust.diary.fragment.note_add;

public interface NoteAddFragmentView {

    void setKeyboardVisible(boolean value);
    String getNoteDescription();

    void onNoteAddClick();
}
