package com.teadust.diary.fragment.note_add;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.teadust.diary.R;
import com.teadust.diary.fragment.base.BaseFragment;
import com.teadust.diary.model.Note;
import com.teadust.diary.utils.DateTime;

public class NoteAddFragment extends BaseFragment implements NoteAddFragmentView {

    private static final String NEW_NOTE_ID = "NEW_NOTE_ID";

    private EditText mDescription;

    public OnNoteAddListener mCallback;

    private NoteAddFragmentPresenter mPresenter;

    /**
     * Interface for interacting between {@link NoteAddFragment} and
     * {@link com.teadust.diary.fragment.note_list.NoteListFragment}.
     */
    public interface OnNoteAddListener {
        void onNoteAddClick(Note note);
    }

    public static NoteAddFragment newInstance(int newNoteId) {
        NoteAddFragment noteAddFragment = new NoteAddFragment();

        Bundle args = new Bundle();
        args.putInt(NEW_NOTE_ID, newNoteId);

        noteAddFragment.setArguments(args);
        return noteAddFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeUI();
        mPresenter = new NoteAddFragmentPresenterImpl();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_note_add, container, false);
        mDescription = (EditText) rootView.findViewById(R.id.note_description);
        mDescription.requestFocus();
        setKeyboardVisible(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_note_add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.note_save) {
            onNoteAddClick();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        setKeyboardVisible(false);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        setDrawerLayoutEnabled(true);
        showDrawerToggle();
        setKeyboardVisible(false);
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnNoteAddListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement "
                    + OnNoteAddListener.class.getName());
        }
    }

    @Override
    public void onDetach() {
        mCallback = null;
        super.onDetach();
    }

    @Override
    public void changeUI() {
        setDrawerLayoutEnabled(false);
        hideDrawerToggle();
        setTitle("Add note");
    }

    @Override
    public void setKeyboardVisible(boolean value) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (value) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } else {
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public String getNoteDescription() {
        return mDescription.getText().toString();
    }

    @Override
    public void onNoteAddClick() {
        Note note = new Note();
        note.setId(getArguments().getInt(NEW_NOTE_ID));
        note.setDescription(getNoteDescription());
        note.setCreationDateTime(new DateTime().getDateTime());

        // Transfer added note to note list adapter.
        mCallback.onNoteAddClick(note);
        // Save added note to database.
        mPresenter.saveNote(note);

        showDrawerToggle();
        setKeyboardVisible(false);
        finishFragment();
    }
}
