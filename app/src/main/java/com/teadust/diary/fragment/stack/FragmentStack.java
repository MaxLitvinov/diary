package com.teadust.diary.fragment.stack;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class FragmentStack {

    private FragmentManager mManager;
    private FragmentTransaction mTransaction;
    private int mContainerViewId;

    public FragmentStack(int containerViewId, FragmentManager manager) {
        mContainerViewId = containerViewId;
        mManager = manager;
    }

    public void showFragment(Fragment fragment) {
        mTransaction = mManager.beginTransaction();
        mTransaction.add(mContainerViewId, fragment);
        mTransaction.commit();
    }

    public void replaceFragment(Fragment fragment) {
        String fragmentBackStackName = fragment.getClass().getName();
        boolean isFragmentPopped = mManager.popBackStackImmediate(fragmentBackStackName, 0);
        if (!isFragmentPopped && mManager.findFragmentByTag(fragmentBackStackName) == null) {
            mTransaction = mManager.beginTransaction();
            mTransaction.replace(mContainerViewId, fragment, fragmentBackStackName);
            mTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            mTransaction.addToBackStack(fragmentBackStackName);
            mTransaction.commit();
        }
    }

    public void finishFragment(Fragment fragment) {
        mManager.popBackStack(fragment.getClass().getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}
