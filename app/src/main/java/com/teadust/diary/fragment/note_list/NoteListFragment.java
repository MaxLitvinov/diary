package com.teadust.diary.fragment.note_list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.teadust.diary.R;
import com.teadust.diary.fragment.base.BaseFragment;
import com.teadust.diary.model.Note;

public class NoteListFragment extends BaseFragment implements NoteListFragmentView {

    private ListView mNoteList;
    private NoteListAdapter mAdapter;

    public static NoteListFragment newInstance() {
        return new NoteListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeUI();
        showNotes();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_note_list, container, false);
        mNoteList = (ListView) rootView.findViewById(R.id.notes_list);
        mNoteList.setAdapter(mAdapter);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_note_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.note_add) {
            mMainView.showNoteAddFragment();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public NoteListAdapter getNoteListAdapter() {
        return mAdapter;
    }

    @Override
    public void showNotes() {
        mAdapter = new NoteListAdapter();
        // Stub.
        for (int i = 0; i < 50; i++) {
            mAdapter.addNote(new Note(i, "Desc " + i, "Date " + i));
        }
    }
}
