package com.teadust.diary.fragment.note_list;

public interface NoteListFragmentView {

    /**
     * Used in {@link com.teadust.diary.fragment.note_add.NoteAddFragment}.
     */
    NoteListAdapter getNoteListAdapter();

    /**
     * Show all notes in {@link NoteListFragment} when application started.
     */
    void showNotes();
}
