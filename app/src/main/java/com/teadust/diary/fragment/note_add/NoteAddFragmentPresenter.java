package com.teadust.diary.fragment.note_add;

import com.teadust.diary.model.Note;

public interface NoteAddFragmentPresenter {

    void saveNote(Note note);
}
