package com.teadust.diary.fragment.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;

import com.teadust.diary.MainView;

public class BaseFragment extends Fragment implements BaseFragmentView {

    protected MainView mMainView;

    /**
     * Variable uses for onResume() method.
     */
    private String mToolbarTitle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mMainView = (MainView) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement " + MainView.class.getName());
        }
    }

    @Override
    public void onDetach() {
        mMainView = null;
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    @Override
    public void changeUI() {

    }

    @Override
    public void setTitle(String toolbarTitle) {
        mToolbarTitle = toolbarTitle;
        mMainView.getToolbar().setTitle(mToolbarTitle);
    }

    @Override
    public void setTitle() {
        setTitle(mToolbarTitle);
    }

    @Override
    public void setDrawerLayoutEnabled(boolean value) {
        if (value) {
            mMainView.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            mMainView.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    @Override
    public void hideDrawerToggle() {
        mMainView.getDrawerToggle().setDrawerIndicatorEnabled(false);
    }

    @Override
    public void showDrawerToggle() {
        mMainView.getDrawerToggle().setDrawerIndicatorEnabled(true);
    }

    @Override
    public void finishFragment() {
        mMainView.getFragmentStack().finishFragment(this);
    }
}
