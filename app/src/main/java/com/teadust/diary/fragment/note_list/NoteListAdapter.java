package com.teadust.diary.fragment.note_list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.teadust.diary.R;
import com.teadust.diary.model.Note;

import java.util.ArrayList;
import java.util.List;

public class NoteListAdapter extends BaseAdapter {

    private List<Note> mNotes;

    public NoteListAdapter() {
        mNotes = new ArrayList<Note>();
    }

    public List<Note> getNotes() {
        return mNotes;
    }

    public void setNotes(List<Note> notes) {
        if (notes != null && notes.size() > 0) {
            mNotes.clear();
            mNotes.addAll(notes);
        }
        notifyDataSetChanged();
    }

    public void addNote(Note note) {
        mNotes.add(note);
        notifyDataSetChanged();
    }

    public void setNote(Note note) {
        int editedNoteId = note.getId();
        mNotes.set(editedNoteId, note);
        notifyDataSetChanged();
    }

    public int getNewNoteId() {
        return mNotes.size();
    }

    @Override
    public int getCount() {
        return mNotes.size();
    }

    @Override
    public Note getItem(int position) {
        return mNotes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        NoteHolder noteHolder;
        if (convertView == null) {
            rootView = inflater.inflate(R.layout.adapter_note_list, parent, false);
            noteHolder = new NoteHolder();
            noteHolder.mDescription = (TextView) rootView.findViewById(R.id.note_description);
            noteHolder.mCreationDate = (TextView) rootView.findViewById(R.id.note_creation_date);
            rootView.setTag(noteHolder);
        } else {
            rootView = convertView;
            noteHolder = (NoteHolder) rootView.getTag();
        }
        noteHolder.mDescription.setText(getItem(position).getDescription());
        noteHolder.mCreationDate.setText(getItem(position).getCreationDateTime());
        return rootView;
    }

    private class NoteHolder {

        TextView mDescription;
        TextView mCreationDate;
    }
}
