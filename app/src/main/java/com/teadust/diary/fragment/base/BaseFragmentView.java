package com.teadust.diary.fragment.base;

import android.support.v4.app.Fragment;

public interface BaseFragmentView {

    /**
     * UI changes depending on what fragment now in use.
     */
    void changeUI();
    /**
     * Method sets title of the Toolbar.
     */
    void setTitle(String toolbarTitle);
    /**
     * Methos sets title of the Toolbar. Uses in {@link BaseFragment} onResume() method when
     * fragment resumed.
     */
    void setTitle();
    void setDrawerLayoutEnabled(boolean value);
    void hideDrawerToggle();
    void showDrawerToggle();
    void finishFragment();
}
