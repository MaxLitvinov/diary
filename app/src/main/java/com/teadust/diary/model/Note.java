package com.teadust.diary.model;

public class Note {

    private int mId;
    private String mDescription;
    private String mCreationDateTime;

    public Note() {
    }

    public Note(int id, String description, String creationDate) {
        mId = id;
        mDescription = description;
        mCreationDateTime = creationDate;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getCreationDateTime() {
        return mCreationDateTime;
    }

    public void setCreationDateTime(String creationDateTime) {
        mCreationDateTime = creationDateTime;
    }
}
