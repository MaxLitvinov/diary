package com.teadust.diary.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

public class NoteDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "NoteDatabase";

    private static class Column implements BaseColumns {
        static final String TABLE_NAME = "table_note";
        static final String ID = "id";
        static final String DESCRIPTION = "description";
        static final String CREATION_DATE_TIME = "creation_date_time";
        static final String[] ROW = {ID, DESCRIPTION, CREATION_DATE_TIME};
    }

    private static class Query {
        static final String CREATE_TABLE = "CREATE TABLE " + Column.TABLE_NAME
                + " (" + Column.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Column.DESCRIPTION + " TEXT, " + Column.CREATION_DATE_TIME + " TEXT)";
        static final String DROP_TABLE = "DROP TABLE IF EXISTS " + CREATE_TABLE;
        static final String GET_ALL_NOTES = "SELECT * FROM " + Column.TABLE_NAME;
    }

    public NoteDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Query.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Query.DROP_TABLE);
    }

    public long addNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Column.DESCRIPTION, note.getDescription());
        values.put(Column.CREATION_DATE_TIME, note.getCreationDateTime());

        long addedNote = db.insert(Column.TABLE_NAME, null, values);

        db.close();
        return addedNote;
    }

    public void deleteNote(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Column.TABLE_NAME, Column.ID + "=?", new String[] {String.valueOf(id)});
        db.close();
    }

    public Note getNote(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(
                Column.TABLE_NAME,
                Column.ROW,
                Column.ID + "=?",
                new String[] {String.valueOf(id)},
                null, null, null, null);

        Note note = new Note();
        if (cursor != null) {
            cursor.moveToFirst();
            note.setId(Integer.parseInt(cursor.getString(0)));
            note.setDescription(cursor.getString(1));
            note.setCreationDateTime(cursor.getString(2));
            cursor.close();
        }

        return note;
    }

    public Note getLastAddedNote() {
        long notesCount = getNotesCount();
        return getNote(notesCount);
    }

    private long getNotesCount() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(Query.GET_ALL_NOTES, null);
        long notesCount = cursor.getCount();
        cursor.close();

        return notesCount;
    }

    public List<Note> getAllNotes() {
        List<Note> notesList = new ArrayList<Note>();

        String selectQuery = "SELECT * FROM " + Column.TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Note note = new Note(
                        cursor.getInt(cursor.getColumnIndex(Column.ID)),
                        cursor.getString(cursor.getColumnIndex(Column.DESCRIPTION)),
                        cursor.getString(cursor.getColumnIndex(Column.CREATION_DATE_TIME)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return notesList;
    }
}
