package com.teadust.diary;

import android.icu.text.LocaleDisplayNames;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.teadust.diary.fragment.stack.FragmentStack;
import com.teadust.diary.fragment.note_add.NoteAddFragment;
import com.teadust.diary.fragment.note_list.NoteListFragment;
import com.teadust.diary.model.Note;

public class MainActivity extends AppCompatActivity implements MainView,
        NavigationView.OnNavigationItemSelectedListener, NoteAddFragment.OnNoteAddListener {

    private static final String TAG = MainActivity.class.getName();

    private Toolbar mToolbar;
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mDrawerToggle;

    private NoteListFragment mNoteListFragment;
    private NoteAddFragment mNoteAddFragment;

    private FragmentStack mFragmentStack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_drawer_note_add) {
            onNoteAddClick();
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public DrawerLayout getDrawerLayout() {
        return mDrawer;
    }

    @Override
    public ActionBarDrawerToggle getDrawerToggle() {
        return mDrawerToggle;
    }

    @Override
    public FragmentStack getFragmentStack() {
        return mFragmentStack;
    }

    @Override
    public void initUI() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        // Initialize content.
        showNoteList();
    }

    @Override
    public void showNoteList() {
        mNoteListFragment = NoteListFragment.newInstance();
        mFragmentStack = new FragmentStack(R.id.content, getSupportFragmentManager());
        mFragmentStack.showFragment(mNoteListFragment);
    }

    @Override
    public void onNoteAddClick() {
        showNoteAddFragment();
    }

    @Override
    public void showNoteAddFragment() {
        int newNoteId = mNoteListFragment.getNoteListAdapter().getNewNoteId();
        mNoteAddFragment = NoteAddFragment.newInstance(newNoteId);
        mFragmentStack.replaceFragment(mNoteAddFragment);
    }

    @Override
    public void onNoteAddClick(Note note) {
        mNoteListFragment.getNoteListAdapter().addNote(note);
    }
}
