package com.teadust.diary.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Uses for note's time creation.
 */
public class DateTime {

    private static final String DATE_PATTERN = "dd.MM.yyyyy HH:mm";

    private DateFormat mDateFormat;

    public DateTime() {
        mDateFormat = new SimpleDateFormat(DATE_PATTERN, Locale.getDefault());
    }

    /**
     * @return Current date and time in "dd.MM.yyyyy HH:mm" format.
     */
    public String getDateTime() {
        String currentDateTime = mDateFormat.format(new Date());
        return currentDateTime;
    }
}
